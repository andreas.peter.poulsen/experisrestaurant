export const groupByInitial = (arr, property) => {
    return arr.reduce((accumulate, obj) => {
        let key = obj[property].substring(0, 1).toLowerCase();
        if (!accumulate[key]) {
            accumulate[key] = [];
        }
        accumulate[key].push(obj);
        return accumulate;
    }, {});
}

export const transformRestaurantObjectToLatLng = (arr) => {
    return arr.reduce((accumulate, obj) => {
        let o = {};
        o.lat = obj.latitude;
        o.lng = obj.longitude;
        accumulate.push(o);
        return accumulate;
    }, []);
}