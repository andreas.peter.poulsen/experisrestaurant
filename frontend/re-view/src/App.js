import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faUtensils,
    faMapMarkerAlt,
    faListOl,
    faSearch,
    faStar,
    faMapMarkedAlt,
    faInfoCircle,
    faUserFriends,
    faGlobeEurope,
    faUser,
    faEnvelope,
    faKey
} from '@fortawesome/free-solid-svg-icons';
import Dashboard from './components/dashboard/Dashboard';
import Login from './components/user/Login';
import Header from './components/common/header/Header';
import Footer from './components/common/footer/Footer';
import Search from './components/search/Search';
import Restaurant from './components/restaurant/Restaurant';
import Restaurants from './components/restaurant/Restaurants';
import Landing from './components/landing/Landing';
import Register from './components/user/Register';
import FourOhFour from './components/common/FourOhFour';

// Preparing fontawesome icons library
library.add(
    faUtensils,
    faMapMarkerAlt,
    faListOl,
    faSearch,
    faStar,
    faMapMarkedAlt,
    faInfoCircle,
    faUserFriends,
    faGlobeEurope,
    faUser,
    faEnvelope,
    faKey
);

function App() {
    return (
        <Router>
            <Header />
            <Switch>
                <Route exact path="/" component={Landing} />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/restaurant/:id" component={Restaurant} />
                <Route path={["/restaurant", "/restaurants"]} component={Restaurants} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/search" component={Search} />
                <Route component={FourOhFour} />
            </Switch>
            <Footer />
        </Router>
    );
}

export default App;
