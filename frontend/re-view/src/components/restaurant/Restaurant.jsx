import React from 'react';
import GoogleMaps from '../maps/GoogleMaps';
import styles from '../../css/Restaurant.module.css';
import landing from '../../css/Landing.module.css';
import RestaurantCard from '../common/RestaurantCard';
import ReviewCard from '../common/ReviewCard';

export default class Restaurant extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            success: false,
            error: false,
            restaurant_name: "",
        }
    }

    componentDidMount() {

        const { id } = this.props.match.params;

        if (id) {
            fetch(`http://localhost:8080/api/restaurant/${id}`)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        restaurant_id: id,
                        success: true,
                        data
                    })
                })
                .catch(error => {
                    this.setState({
                        success: false,
                        error: true,
                    })
                })
        }
    }

    createRestaurant(data) {
        return (
            <>
                <div className={styles.mapContainer}>
                    <GoogleMaps markers={[{ lat: data.latitude, lng: data.longitude }]} />
                </div>
                <RestaurantCard data={data} restaurantLink={false} className={styles.info} />
                <div className={styles.reviews}>
                    <h2>Reviews</h2>
                    {data.reviews.map((review, index) => {
                        return <ReviewCard data={review} key={index} reviewLink={false} />;
                    })}
                </div>
            </>
        );
    }

    render() {

        const { success, restaurant_id, error, data } = this.state;

        return (
            <div className={landing.container}>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {
                    <div className={styles.module}>
                        {restaurant_id && success && this.createRestaurant(data)}
                    </div>
                }
            </div>
        )
    }
}