import React from 'react';
import styles from '../../css/Landing.module.css';
import RestaurantCard from '../common/RestaurantCard';

export default class Restaurants extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            data: []
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/api/restaurants`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    data: data,
                    success: true,
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    render() {

        const { success, error, data } = this.state;

        return (
            <div className={styles.container}>
                <h2>All restaurants</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && <div>
                    {data.map((restaurant, index) => {
                        return <div className={styles.restaurantCard} key={index}><RestaurantCard data={restaurant} key={index} restaurantLink={true} /></div>;
                    })}
                </div>
                }
            </div>
        )
    }
}