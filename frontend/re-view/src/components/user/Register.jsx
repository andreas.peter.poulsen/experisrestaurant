import React from 'react';
import styles from '../../css/Login.module.css';
import { Redirect } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            email: "",
            success: false,
            error: false,
            data: {}
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let obj = {};

        obj.username = this.state.username;
        obj.password = this.state.password;
        obj.email = this.state.email;
        obj.active = true;
        obj.role = 1;

        fetch(`http://localhost:8080/api/user/create`, {
            method: "POST",
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj)
        })
            .then(response => {
                return response.json()
            })
            .then(data => {
                this.setState({
                    success: true,
                    data
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    render() {

        const { success, error, data } = this.state;
        return (
            <>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {success && <Redirect to={{ pathname: '/login', state: data }} />}
                <div className={styles.module}>
                    <form onSubmit={this.handleSubmit}>
                        <div className={styles.username}>
                            <span className={styles.iconContainer}>
                                <FontAwesomeIcon icon="user" />
                            </span>
                            <input onChange={this.handleChange} name="username" type="text" placeholder="username" />
                        </div>
                        <div className={styles.email}>
                            <span className={styles.iconContainer}>
                                <FontAwesomeIcon icon="envelope" />
                            </span>
                            <input onChange={this.handleChange} name="email" type="email" placeholder="email" />
                        </div>
                        <div className={styles.password}>
                            <span className={styles.iconContainer}>
                                <FontAwesomeIcon icon="key" />
                            </span>
                            <input onChange={this.handleChange} name="password" type="password" placeholder="password" />
                        </div>
                        <button className={styles.button} type="submit">Register</button>
                    </form>
                </div>
            </>
        )
    }
}