import React from 'react';
import styles from '../../../css/Header.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

export default class Logo extends React.Component {
    render() {
        return (
            <Link to="/"><p className={styles.logo}><FontAwesomeIcon icon="utensils" />  Re-View</p></Link>
        )
    }
}