import React from 'react';
import styles from '../../../css/Header.module.css';
import { Link } from 'react-router-dom';

export default class UserAuthorizationHandler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }

    componentDidMount() {
        if (sessionStorage.getItem("user")) {
            this.setState({user: JSON.parse(sessionStorage.getItem("user"))})
        }
    }

    render() {
        const { user } = this.state;

        return (
            <>
                {!user.username ?
                    <div className={styles.login}>
                        <Link to="/login" className={styles.link}>Login</Link>
                        <Link to="/register" className={styles.link}>Register</Link>
                    </div> :
                    <div className={styles.login}>
                        <Link to="/dashboard" className={styles.link}>Dashboard</Link>
                    </div>
                }
            </>
        )
    }
}