import React from 'react';
import Logo from './Logo';
import UserAuthorizationHandler from './UserAuthorizationHandler';
import styles from '../../../css/Header.module.css';
import { Link } from 'react-router-dom';
import Searchbar from '../Searchbar';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div>
                <div className={styles.module}>
                    <Logo />
                    <div className={styles.links}>
                        <Link to="/">Home</Link>
                        <Link to="/restaurants">Restaurants</Link>
                    </div>
                    <UserAuthorizationHandler />
                </div>
                <Searchbar
                    className={styles.search}
                    placeholder="Search restaurants"
                />
            </div>
        )
    }
}