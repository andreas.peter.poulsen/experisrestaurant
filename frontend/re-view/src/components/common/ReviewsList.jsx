import React from 'react';
import ReviewCard from './ReviewCard';


export default class ReviewsList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            data: []
        }
    }

    componentDidMount() {
        if (this.props.userId) {
            fetch(`http://localhost:8080/api/review/user/${this.props.userId}`)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        success: true,
                        data
                    })
                })
                .catch(error => {
                    this.setState({
                        success: false,
                        error: true,
                    })
                })
        }
        if (this.props.restaurantId) {
            fetch(`http://localhost:8080/api/restaurant/${this.props.restaurantId}`)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        success: true,
                        data
                    })
                })
                .catch(error => {
                    this.setState({
                        success: false,
                        error: true,
                    })
                })
        }
    }

    render() {

        const { success, error, data } = this.state;

        console.log(this.props.userId, this.props.restaurantId);

        return (
            <div className={this.props.className}>
                <h2>Reviews</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && <div>
                    {this.props.userId && data.map((review, index) => {
                        return <ReviewCard data={review} key={index} />;
                    })}
                     {this.props.restaurantId && data.map((review, index) => {
                        return <ReviewCard data={review} key={index} />;
                    })}
                </div>
                }
            </div>
        )
    }
}