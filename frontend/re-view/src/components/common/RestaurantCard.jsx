import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import styles from '../../css/RestaurantCard.module.css';

export default class Restaurants extends React.Component {

    render() {

        const { data, index, restaurantLink } = this.props;

        return (
            <div key={ index } className={styles.module}>
                <h2>{
                    restaurantLink ? 
                    <Link to={`/restaurant/${data.id}`}><strong>{data.restaurant_name}</strong></Link> :
                    <strong>{data.restaurant_name}</strong>
                }</h2>
                <p><FontAwesomeIcon icon="star" /> {data.averageRating ? data.averageRating : "N/A"}</p>
                <p><FontAwesomeIcon icon="map-marked-alt" /> {data.address}</p>
                <p><FontAwesomeIcon icon="info-circle" /> {data.description}</p>
                <p><FontAwesomeIcon icon="user-friends" /> {data.targetAudience}</p>
                <p><FontAwesomeIcon icon="utensils" /> {data.main_food}</p>
                <p><FontAwesomeIcon icon="globe-europe" /> {data.theme}</p>
            </div>
        );
    }
}