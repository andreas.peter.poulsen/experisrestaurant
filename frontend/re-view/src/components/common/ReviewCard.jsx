import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../../css/Landing.module.css';
import { Link } from 'react-router-dom';

export default class ReviewCard extends React.Component {

    render() {

        const { data, index, reviewLink } = this.props;

        return (
            <div key={index} className={styles.reviewCard}>
                {reviewLink ? 
                    <Link to={`/restaurant/${data.restaurant_id_fk}`}>
                        <p><FontAwesomeIcon icon="star" /> {data.rating}</p>
                        <p className={styles.review}>{data.review}</p>
                        <p className={styles.reviewer}> - {data.peep.username}</p>
                    </Link> :
                    <>
                        <p><FontAwesomeIcon icon="star" /> {data.rating}</p>
                        <p className={styles.review}>{data.review}</p>
                        <p className={styles.reviewer}> - {data.peep.username}</p>
                    </>
                }

            </div>
        )
    }
}