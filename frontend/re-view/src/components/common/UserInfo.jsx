import React from 'react';
import styles from '../../css/UserInfoBox.module.css';
import button from '../../css/Button.module.css';

export default class UserInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: "",
            currentData: null,
            username: "",
            password: "",
            email: ""
        }
    }

    componentDidMount() {
        // Fetch to get user info here. Was going to be used, but we have no login in backend, so won't bother finishing.


        // Fetch för att hämta och fylla i nuvarande värden (som placeholders?).
        // this.props.userId
        // fetch(`/user/update`)
        // .then(response => response.json())
        // .then(data => this.setState({currentData: data}))
    }

    handleSubmit = event => {
        // fetch(`/user/update`)
            // .then(response => response.json())
            // .then(data => this.setState({data: data}))
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return (
            <div className={styles.module + " " + this.props.className}>
                <input 
                    type="text"
                    name="username"
                    placeholder="username"
                    onChange={this.handleChange}
                />
                <input
                    type="password"
                    name="password"
                    placeholder="password"
                    onChange={this.handleChange}
                />
                <input
                    type="text"
                    name="email"
                    placeholder="email"
                    onChange={this.handleChange}
                />
                <button 
                    className={button.module}
                    onClick={this.handleSubmit}
                >
                    Change
                </button>
            </div>
        )
    }
}