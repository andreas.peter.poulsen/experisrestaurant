import React from 'react';

export default class FourOhFour extends React.Component {

    render() {
     
        return (
            <div>
               <p>404. Nothing here :(</p>
            </div>
        )
    }
}