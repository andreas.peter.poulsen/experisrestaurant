import React from 'react';
import styles from '../../../css/Footer.module.css';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            spin: false,
        }
    }

    handleClick = event => {
        this.setState({
            spin: !this.state.spin,
        });
    }

    render() {
        return (
            <p onClick={this.handleClick} className={styles.module + " " + (this.state.spin ? styles.spin : "")}><span role="img" aria-label="rainbow">🌈</span> &copy; 2019 Team awsome power ponnies <span role="img" aria-label="unicorn">🦄</span></p>
        )
    }
}