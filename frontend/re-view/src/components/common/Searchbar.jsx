import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../../css/Searchbar.module.css';
import button from '../../css/Button.module.css';
import { Redirect } from 'react-router-dom';

export default class Searchbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            keyword: "",
            success: false,
            error: false,
            data: [],
            toSearch: false
        }
    }

    handleSubmit = event => {
        event.preventDefault();
        fetch(`http://localhost:8080/api/restaurants/${this.state.keyword}`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    success: true,
                    data: data,
                    toSearch: true
                }, () => {
                    // Ugly hack to reset the state of the search to prevent the component redirecting to search page if updated
                    setTimeout(() => { this.setState({ toSearch: false }) }, 10);
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    handleChange = event => {
        this.setState({ keyword: event.target.value })
    }

    createSearchbar() {
        return (
            <div className={styles.module + " " + this.props.className}>
                <form onSubmit={this.handleSubmit}>
                    <input
                        className={styles.input}
                        type="text"
                        placeholder={this.props.placeholder}
                        onChange={this.handleChange}
                    />
                    <button
                        className={styles.button + " " + button.module}
                        onClick={this.handleSubmit}
                    >
                        <FontAwesomeIcon icon="search" /> Search
                    </button>
                </form>
            </div>
        )
    }

    // componentDidMount() {
    //     if (this.state.toSearch) {
    //         this.setState({toSearch: false})
    //     }
    // }

    render() {

        if (this.state.toSearch) {
            return (
                <>
                    <Redirect to={{ pathname: '/search', state: this.state }} />
                    {/* The following repeated to make sure the searchbar is always visible, even if the redirect happened */}
                    {this.createSearchbar()}
                </>
            )
        }
        return this.createSearchbar();
    }
}