import React from 'react';
import UserInfo from '../common/UserInfo';
import styles from '../../css/Dashboard.module.css';
import ReviewsList from '../common/ReviewsList';
import RestaurantsList from '../common/RestaurantsList';

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        }
    }

    componentDidMount() {
        if (sessionStorage.getItem("user")) {
            this.setState({user: JSON.parse(sessionStorage.getItem("user"))})
        }
    }

    render() {

        const { user } = this.state;

        return (
            <div className={styles.module}>
                <h1 className={styles.h1}>Dashboard {user.username ? `- ${user.username}` : ""}</h1>
                <UserInfo className={styles.infoBox} userId={1} />
                {user.role === 1 ? <ReviewsList className={styles.reviews} userId={user.id} /> : ""}
                {/* {user.role === 2 ? <ReviewsList className={styles.reviews} restaurantId={1} /> : ""}
                {user.role === 2 ? <RestaurantsList className={styles.restaurants} userId={user.id} /> : ""} */}
            </div>
        )
    }
}