import React from 'react';
import RestaurantCard from '../common/RestaurantCard';
import styles from '../../css/Landing.module.css';

export default class Search extends React.Component {
    createResult(restaurant, index) {
        return (
            <div className={styles.restaurantCard} key={index}>
                <RestaurantCard key={index} data={restaurant} restaurantLink={true} />
            </div>
        )
    }

    render() {

        const { error, success, data } = this.props.location.state;

        return (
            <div className={styles.container}>
                <h2>Search</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && data.length > 0 && data.map((restaurant, index) => {
                    return this.createResult(restaurant, index);
                })}
                {data.length === 0 && <p>No search results found for keyword '{this.props.location.state.keyword}'</p>}
            </div>
        )
    }
}