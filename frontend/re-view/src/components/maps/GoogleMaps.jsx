import React from 'react';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import styles from '../../css/GoogleMaps.module.css';
import mapStyles from '../../data/style.json';

export default class GoogleMaps extends React.Component {

    createMarker(marker, index) {
        return (
            <Marker key={index} position={marker} />
        )
    }

    render() {

        const { markers } = this.props;

        return (
            <LoadScript id="script-loader" googleMapsApiKey={process.env.REACT_APP_MAPS_KEY}>
                <GoogleMap
                    id="gmap"
                    mapContainerClassName={styles.mapContainer}
                    options={{ styles: mapStyles }}
                    zoom={14}
                    onLoad={map => {
                        const bounds = new window.google.maps.LatLngBounds();
                        markers.map(marker => {
                            return bounds.extend(marker);
                        })
                        map.fitBounds(bounds);
                    }}
                >
                    {markers.map((marker, index) => {
                        return this.createMarker(marker, index);
                    })}
                </GoogleMap>
            </LoadScript>
        )
    }
}