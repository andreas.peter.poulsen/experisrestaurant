import React from 'react';
import styles from '../../css/Landing.module.css';
import RestaurantCard from '../common/RestaurantCard';

export default class Ratings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            data: []
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/api/restaurants/ratings`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    success: true,
                    data
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    createRestaurantCard(restaurant, index) {
        return (
            <div key={index} className={styles.restaurantCard}>
                <RestaurantCard data={restaurant} restaurantLink={true} />
            </div>
        )
    }

    render() {

        const { error, success, data } = this.state;

        return (
            <>
                <h2>Top rated restaurants</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && <div>
                    {data.map((restaurant, index) => {
                        return this.createRestaurantCard(restaurant, index);
                    })}
                </div>
                }
            </>
        )
    }
}