import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../../css/RestaurantBrowser.module.css';
import parent from '../../css/Landing.module.css';
import Ratings from './Ratings';
import Types from './Types';
import Locations from './Locations';

export default class RestaurantsBrowser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: "ratings",
        }
    }

    handleClick = event => {
        this.setState({
            activeTab: event.target.name
        });
    }

    render() {
        const { activeTab } = this.state;

        return (
            <div className={parent.restaurantsBrowser}>
                <div className={styles.filter}>
                    <button
                        onClick={this.handleClick}
                        name="ratings"
                        className={styles.link + " " + (activeTab === "ratings" ? styles.current : "")}
                    ><FontAwesomeIcon icon="list-ol" /> Ratings</button>

                    <button
                        onClick={this.handleClick}
                        name="types"
                        className={styles.link + " " + (activeTab === "types" ? styles.current : "")}
                    ><FontAwesomeIcon icon="utensils" /> Types</button>

                    <button
                        onClick={this.handleClick}
                        name="locations"
                        className={styles.link + " " + (activeTab === "locations" ? styles.current : "")}
                    ><FontAwesomeIcon icon="map-marker-alt" /> Locations</button>
                </div>
                <div className={styles.content}>
                    {activeTab === "ratings" && <Ratings />}
                    {activeTab === "types" && <Types />}
                    {activeTab === "locations" && <Locations />}
                </div>
            </div>
        )
    }
}