import React from 'react';
import styles from '../../css/Landing.module.css';
import ReviewCard from '../common/ReviewCard';

export default class RecentReviews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            data: []
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/api/reviews/latest`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    success: true,
                    data
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    render() {

        const { error, success, data } = this.state;

        return (
            <div className={styles.container + " " + styles.recent}>
                <h2>Recent reviews</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && <div>
                    {data.map((review, index) => {
                        return <ReviewCard data={review} key={index} reviewLink={true} />
                    })}
                </div>
                }
            </div>
        )
    }
}