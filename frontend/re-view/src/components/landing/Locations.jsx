import React from 'react';
import GoogleMaps from '../maps/GoogleMaps';
import styles from '../../css/Landing.module.css';
import { transformRestaurantObjectToLatLng } from '../../functions/reduce';

export default class Locations extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/api/restaurants`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    success: true,
                    data: transformRestaurantObjectToLatLng(data)
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    render() {

        const { error, success, data } = this.state;

        return (
            <>
                <h2>Restaurants by Location</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && <div className={styles.location} ><GoogleMaps markers={data} /></div>}
            </>
        )
    }
}