import React from 'react';
import { groupByInitial } from '../../functions/reduce';
import styles from '../../css/Landing.module.css';
import { Link } from 'react-router-dom';

export default class Types extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            reducedData: null
        }
    }

    componentDidMount() {
        fetch(`http://localhost:8080/api/restaurants/types`)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    reducedData: groupByInitial(data, "theme"),
                    success: true,
                })
            })
            .catch(error => {
                this.setState({
                    success: false,
                    error: true,
                })
            })
    }

    createTypes(key, arr, index) {
        return (
            <div key={index}>
                <h2 className={styles.initial}>{key.toUpperCase()}</h2>
                {arr.map((restaurant, index) => {
                    return (
                        <div key={index} className={styles.restaurantCard}>
                            <h2>{restaurant.theme}</h2>
                            <p><Link to={`/restaurant/${restaurant.id}`}><strong>{restaurant.restaurant_name}</strong></Link> - {restaurant.address}</p>
                            <p>{restaurant.description}</p>
                            <p>{restaurant.main_food}</p>
                        </div>
                    )
                })}
            </div>
        )
    }

    render() {

        const { error, success, reducedData } = this.state;

        return (
            <>
                <h2>Restaurants by types</h2>
                {error && <p>There was an error while trying to retrieve data, please try again.</p>}
                {!success && !error && <p>Attempting to fetch data...</p>}
                {success && reducedData && <div>
                    {Object.keys(reducedData).map((key, index) => {
                        return this.createTypes(key, reducedData[key], index);
                    })}
                </div>
                }
            </>
        )
    }
}