import React from 'react';
import styles from '../../css/Landing.module.css';
import RecentReviews from './RecentReviews';
import RestaurantsBrowser from './RestaurantsBrowser';

export default class Landing extends React.Component {

    render() {
        return (
            <div className={styles.module}>
                <RestaurantsBrowser />
                <RecentReviews />
            </div>
        )
    }
}