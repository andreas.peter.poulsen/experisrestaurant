package se.experis.restaurantAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.restaurantAPI.models.Restaurant;

import java.util.List;

public interface RestaurantRep extends JpaRepository<Restaurant, Integer> {
    Restaurant findRestaurantById(int id);
    List<Restaurant> findAllByOrderByThemeAsc();
    List<Restaurant> findTop5ByAverageRatingNotNullOrderByAverageRatingDesc();

    List<Restaurant> findAllByAddressIgnoreCaseContainingOrDescriptionIgnoreCaseContainingOrRestaurantnameIgnoreCaseContainingOrMainfoodIgnoreCaseContaining(String name,String name2,String name3,String name4);
}
