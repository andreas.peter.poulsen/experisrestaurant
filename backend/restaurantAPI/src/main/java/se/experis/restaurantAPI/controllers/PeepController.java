package se.experis.restaurantAPI.controllers;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;
import se.experis.restaurantAPI.models.Peep;
import se.experis.restaurantAPI.repository.PeepRep;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PeepController {
    @Autowired
    public PeepRep peepRep;

    public PeepController(PeepRep peepRep){
        this.peepRep = peepRep;
    }

    @RequestMapping("/api/user/create")
    @ResponseBody
    public Peep addNewUser(@RequestBody Peep peep) {
        Peep p = null;
        try {
            p = peepRep.save(peep);
        } catch(Exception e) {
            System.out.println(e);
        }

        return p;
    }

    @GetMapping("/api/users")
    public Iterable<Peep> getAllUsers(){
        return peepRep.findAll();
    }

    @GetMapping("/api/user/{id}")
    public Peep getUser(@PathVariable int id){
        return peepRep.findPeepById(id);
    }


    @PostMapping("/api/user/login")
    public Peep login(@RequestBody String data) {

        JSONObject dataobj = new JSONObject(data);

        String plainPassword = dataobj.getString("password");
        String username = dataobj.getString("username");
        Peep result = null;

        try{
            Peep newp = peepRep.findByUsername(username);

            if (BCrypt.checkpw(plainPassword, newp.getPassword())){
                System.out.println("The password matches.");
                newp.setPassword("");
                result = newp;
            } else{
                System.out.println("The password does not match.");
            }
        } catch (Exception e){
            System.out.println(e);
        }

        return result;
    }
}
