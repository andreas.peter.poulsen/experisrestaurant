package se.experis.restaurantAPI.controllers;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.experis.restaurantAPI.models.Peep;
import se.experis.restaurantAPI.models.Restaurant;
import se.experis.restaurantAPI.repository.PeepRep;
import se.experis.restaurantAPI.repository.RestaurantRep;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class RestaurantController {

    @Autowired
    public RestaurantRep restaurantrep;
    public PeepRep peeprep;

    public RestaurantController(RestaurantRep restaurantrep, PeepRep peeprep) {
        this.restaurantrep = restaurantrep;
        this.peeprep = peeprep;
    }

    @PostMapping("/api/restaurant/create")
    public Restaurant addNewRestaurant(@RequestBody Restaurant restaurant) {
        Restaurant rest = restaurantrep.save(restaurant);
        return rest;
    }

    @PostMapping("/api/restaurant/update")
    public String updateRestaurant(@RequestBody Restaurant restaurant) {
        restaurantrep.save(restaurant);
        return "restaurant updated";
    }

    @GetMapping("/api/restaurant/{id}")
    public Restaurant getSpecificRestaurants(@PathVariable int id){
        return restaurantrep.findRestaurantById(id);
    }

    @GetMapping("/api/restaurants")
    public Iterable<Restaurant> getAllRestuarants(){
        return restaurantrep.findAll();
    }

    @GetMapping("/api/restaurants/types")
    public Iterable<Restaurant> getRestaurantsTypes() {
        return restaurantrep.findAllByOrderByThemeAsc();
    }

    @GetMapping("/api/restaurants/ratings")
    public Iterable<Restaurant> getRestaurantsByTopRating() {
        return restaurantrep.findTop5ByAverageRatingNotNullOrderByAverageRatingDesc();
    }

    @GetMapping("/api/restaurants/{name}")
    public Iterable<Restaurant> GetAllContains(@PathVariable String name){
        return restaurantrep.findAllByAddressIgnoreCaseContainingOrDescriptionIgnoreCaseContainingOrRestaurantnameIgnoreCaseContainingOrMainfoodIgnoreCaseContaining(name,name,name,name);
    }

}
