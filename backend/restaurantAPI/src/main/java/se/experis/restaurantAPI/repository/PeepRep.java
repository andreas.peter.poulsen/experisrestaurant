package se.experis.restaurantAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.restaurantAPI.models.Peep;

public interface PeepRep extends JpaRepository<Peep, Integer> {
    Peep findPeepById(int id);
    Peep findByUsername(String one);
    Peep findByPassword(String one);
}
