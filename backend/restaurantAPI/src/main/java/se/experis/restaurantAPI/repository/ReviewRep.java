package se.experis.restaurantAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.restaurantAPI.models.Review;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface ReviewRep extends JpaRepository<Review, Integer> {
    Review findReviewById(int id);
    List<Review> findTop5ByOrderByUpdatedAtDesc();
    List<Review> findAllByUserIdFk(int id);
}
