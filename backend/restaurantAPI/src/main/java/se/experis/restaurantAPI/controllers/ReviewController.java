package se.experis.restaurantAPI.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import se.experis.restaurantAPI.models.Review;
import se.experis.restaurantAPI.repository.ReviewRep;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ReviewController {
    @Autowired
    private final ReviewRep reviewRep;

    public ReviewController(ReviewRep reviewRep) {
        this.reviewRep = reviewRep;
    }

    @GetMapping("/api/review/{id}")
    public Review getReview(@PathVariable("id") int id) {
        return reviewRep.findReviewById(id);
    }

    @GetMapping("/api/review/user/{id}")
    public List<Review> getReviewByUser(@PathVariable("id") int id) {
        return reviewRep.findAllByUserIdFk(id);
    }

    @PostMapping("/api/review/create")
    public Review createReview(@RequestBody Review review) {
        return reviewRep.save(review);
    }

    @PostMapping("/api/review/update")
    public String updateReview(@RequestBody Review review) {
        reviewRep.save(review);
        return "review updated";
    }

    @GetMapping("/api/reviews")
    public Iterable<Review> getAllReviews(){
        return reviewRep.findAll();
    }

    @GetMapping("/api/reviews/latest")
    public Iterable<Review> getLatestReviews(){
        return reviewRep.findTop5ByOrderByUpdatedAtDesc();
    }
}
